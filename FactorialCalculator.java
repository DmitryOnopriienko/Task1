import java.util.Scanner;

public class FactorialCalculator {
    public static int factorial(int num) {
        if (num == 0)
            return 1;
        else
            return num * factorial(num - 1);
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("error");
            System.exit(1);
        }
        int num = Integer.valueOf(args[0]);
        if (num <= 0) {
            System.out.println("error");
            System.exit(1);
        }

        if (num >= 1 && num < 10)
            System.out.println(factorial(num));
        else {
            System.out.println("Operation is time-consuming. User confirmation is needed.");
            System.out.println("Press 1 to confirm...");
            Scanner confirmationScanner = new Scanner(System.in);
            int confirmation = confirmationScanner.nextInt();
            confirmationScanner.close();
            if (confirmation == 1)
                System.out.println(factorial(num));
            else 
                System.exit(0);
        }
    }
}
