public class PrimeNumbersCalculator {
    public static boolean testIfPrime(int num) {
        boolean result = true;
        for (int i = 2; i <= num / 2; i++) {
            if (num % i == 0) {
                result = false;
                break;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        if (args.length == 0) {
            System.out.println("error");
            System.exit(1);
        }
        int num = Integer.valueOf(args[0]);
        if (num <= 0) {
            System.out.println("error");
            System.exit(1);
        }
        for (int i = 1; i < num; i++) {
            if (testIfPrime(i)) {
                System.out.println(i);
            }
        }
    }
}