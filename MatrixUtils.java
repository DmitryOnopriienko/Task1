import java.util.Scanner;
import java.util.ArrayList;

public class MatrixUtils {

    public static void fillFunc(int matrix[][]) {
        int rows = matrix.length;
        int columns = matrix[0].length;
        int filler = 1;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                matrix[i][j] = filler;
                filler++;
            }
        }
    }

    public static boolean isSquare(int n) {
        double root = Math.sqrt((double)n);
        return (n == (int)root * (int)root);
    }

    public static void main(String[] args) {
        Scanner userInput = new Scanner(System.in);
        System.out.println("Give me a number of rows:");
        int rows = userInput.nextInt();
        System.out.println("Give me a number of columns:");
        int columns = userInput.nextInt();
        userInput.close();
        int[][] matrix = new int[rows][columns];

        fillFunc(matrix);

        ArrayList<Integer> roots = new ArrayList<>();
        int sumOfRoots = 0;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (isSquare(matrix[i][j])) {
                    int root = (int) Math.sqrt((double) matrix[i][j]);
                    roots.add(root);
                    System.out.printf("cell[%d,%d] has value %d, it's root: %d\n",
                            i, j, matrix[i][j], root);
                    sumOfRoots += root;
                }
            }
        }
        System.out.print("Total: ");
        while (roots.isEmpty() == false) {
            System.out.print(roots.remove(roots.size() - 1));
            if (roots.isEmpty() == false)
                System.out.print("+");
            else
                System.out.print(" = ");
        }

        System.out.println(sumOfRoots);
    }
}
